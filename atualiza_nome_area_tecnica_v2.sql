  -- falta apontamento de fronteira

  SET SERVEROUTPUT ON SIZE UNLIMITED;
  SET DEFINE OFF;
  DECLARE
      l_nome_antigo VARCHAR(255);
      l_nome_novo VARCHAR(255);
      l_count NUMBER;
      
      CURSOR cDemanda IS
          SELECT r.request_id 
            FROM kcrt_requests r
                 JOIN kcrt_req_header_details rhd 
                   ON r.request_id = rhd.request_id
                 JOIN kcrt_request_types rt 
                   ON rt.request_type_id = r.request_type_id
           WHERE (r.project_code in ('DEMANDA_INTERNA','DEMANDA_NEGOCIO')
                 OR rt.reference_code = 'TIM_DEMANDA_BASELINE')
                 AND '#@#' ||  rhd.visible_parameter41 || '#@#' like '%#@#' || l_nome_antigo || '#@#%';
               
      CURSOR cFronteira IS --TIM - Fronteira
          SELECT r.request_id
          from kcrt_requests r
               join kcrt_request_details rd1
                 on r.request_id = rd1.request_id and batch_number = 1
               join kcrt_request_types rt
                 on r.request_type_id = rt.request_type_id
          where '#@#' || rd1.visible_parameter1 || '#@#' like '%#@#' || l_nome_antigo || '#@#%'
                AND rt.reference_code = 'TIM_FRONTEIRA';
      
      CURSOR cAreaLiderTecnico IS
          select r.request_id
            from kcrt_requests r
               join kcrt_req_header_details rhd 
                 on r.request_id = rhd.request_id
               join kcrt_request_types rt 
                 on rt.request_type_id = r.request_type_id
          where (r.project_code in ('DEMANDA_INTERNA','DEMANDA_NEGOCIO')
                or rt.reference_code = 'TIM_DEMANDA_BASELINE')
                AND '#@#' || rhd.visible_parameter14 || '#@#' like '%#@#' || l_nome_antigo || '#@#%';
      
      CURSOR cValoracao IS          
          select r.request_id
            from kcrt_requests r
               join kcrt_req_header_details rhd 
                 on r.request_id = rhd.request_id
               join kcrt_request_types rt 
                 on rt.request_type_id = r.request_type_id
          where rt.reference_code = 'TIM_VALORACAO'
                AND '#@#' || rhd.visible_parameter3 || '#@#' like '%#@#' || l_nome_antigo || '#@#%';
      
      CURSOR cBI IS
          select r.request_id
            from kcrt_requests r
                 join kcrt_req_header_details rhd on r.request_id = rhd.request_id
                 join kcrt_request_types rt on rt.request_type_id = r.request_type_id
      where rt.reference_code = 'TIM_BI'
      AND rhd.visible_parameter3 = l_nome_antigo;


      CURSOR cApontamento IS          
          select r.request_id
            from kcrt_requests r
               join kcrt_req_header_details rhd 
                 on r.request_id = rhd.request_id
               join kcrt_request_types rt 
                 on rt.request_type_id = r.request_type_id
          where rt.reference_code = 'TIM_APONTAMENTO_FRONTEIRA'
                AND '#@#' || rhd.visible_parameter3 || '#@#' like '%#@#' || l_nome_antigo || '#@#%';
      
  BEGIN
      l_nome_antigo := 'DIG - Big Data Analysis & Data Governance';
      l_nome_novo := 'DIG - NBA/RTDM';
      
      l_count := 0;
      FOR cDemanda_rec IN cDemanda
      LOOP
          
          UPDATE kcrt_req_header_details rhd 
             SET rhd.visible_parameter41 = replace(rhd.visible_parameter41,l_nome_antigo,l_nome_novo),
                 rhd.last_updated_by = 1,
                 rhd.last_update_date = sysdate            
          WHERE rhd.request_id = cDemanda_rec.request_id;
          
          UPDATE kcrt_requests r
             SET r.last_updated_by = 1,
                 r.last_update_date = sysdate
           WHERE r.request_id = cDemanda_rec.request_id;
           
          UPDATE kcrt_request_details rd
             SET rd.last_updated_by = 1,
                 rd.last_update_date = sysdate
           WHERE rd.request_id = cDemanda_rec.request_id;
          
          l_count := l_count + 1;        
      END LOOP;
      dbms_output.put_line(TO_CHAR(l_count) || ' demandas atualizadas.');
      
      l_count := 0;
      FOR cFronteira_rec IN cFronteira
      LOOP
          
          UPDATE kcrt_request_details rd1
             SET rd1.visible_parameter1 = replace(rd1.visible_parameter1,l_nome_antigo,l_nome_novo)
           WHERE rd1.batch_number = 1
                 AND rd1.request_id = cFronteira_rec.request_id;
          
          UPDATE kcrt_req_header_details rhd 
             SET rhd.last_updated_by = 1,
                 rhd.last_update_date = sysdate            
          WHERE rhd.request_id = cFronteira_rec.request_id;
          
          UPDATE kcrt_requests r
             SET r.last_updated_by = 1,
                 r.last_update_date = sysdate
           WHERE r.request_id = cFronteira_rec.request_id;
           
          UPDATE kcrt_request_details rd
             SET rd.last_updated_by = 1,
                 rd.last_update_date = sysdate
           WHERE rd.request_id = cFronteira_rec.request_id;
          
          l_count := l_count + 1;        
      END LOOP;
      dbms_output.put_line(TO_CHAR(l_count) || ' fronteiras atualizadas.');
      
      l_count := 0;
      FOR cAreaLiderTecnico_rec IN cAreaLiderTecnico
      LOOP        
          UPDATE kcrt_req_header_details rhd 
             SET rhd.visible_parameter14 = replace(rhd.visible_parameter14,l_nome_antigo,l_nome_novo),
                 rhd.last_updated_by = 1,
                 rhd.last_update_date = sysdate            
          WHERE rhd.request_id = cAreaLiderTecnico_rec.request_id;
          
          UPDATE kcrt_requests r
             SET r.last_updated_by = 1,
                 r.last_update_date = sysdate
           WHERE r.request_id = cAreaLiderTecnico_rec.request_id;
           
          UPDATE kcrt_request_details rd
             SET rd.last_updated_by = 1,
                 rd.last_update_date = sysdate
           WHERE rd.request_id = cAreaLiderTecnico_rec.request_id;
          
          l_count := l_count + 1;        
      END LOOP;
      dbms_output.put_line(TO_CHAR(l_count) || ' áreas de líder técnico atualizadas.');
      
      l_count := 0;
      FOR cValoracao_rec IN cValoracao
      LOOP
          
          UPDATE kcrt_req_header_details rhd 
             SET rhd.visible_parameter3 = replace(rhd.visible_parameter3,l_nome_antigo,l_nome_novo),
                 rhd.last_updated_by = 1,
                 rhd.last_update_date = sysdate            
           WHERE rhd.request_id = cValoracao_rec.request_id;
          
          UPDATE kcrt_requests r
             SET r.last_updated_by = 1,
                 r.last_update_date = sysdate
           WHERE r.request_id = cValoracao_rec.request_id;
           
          UPDATE kcrt_request_details rd
             SET rd.last_updated_by = 1,
                 rd.last_update_date = sysdate
           WHERE rd.request_id = cValoracao_rec.request_id;
          
          l_count := l_count + 1;              
      END LOOP;
      dbms_output.put_line(TO_CHAR(l_count) || ' valorações atualizadas.');
      
      l_count := 0;
      FOR cBI_rec IN cBI
      LOOP
          
          UPDATE kcrt_req_header_details rhd 
             SET rhd.visible_parameter3 = replace(rhd.visible_parameter3,l_nome_antigo,l_nome_novo),
                 rhd.last_updated_by = 1,
                 rhd.last_update_date = sysdate            
           WHERE rhd.request_id = cBI_rec.request_id;
          
          UPDATE kcrt_requests r
             SET r.last_updated_by = 1,
                 r.last_update_date = sysdate
           WHERE r.request_id = cBI_rec.request_id;
           
          UPDATE kcrt_request_details rd
             SET rd.last_updated_by = 1,
                 rd.last_update_date = sysdate
           WHERE rd.request_id = cBI_rec.request_id;
          
          l_count := l_count + 1;        
      END LOOP;
      dbms_output.put_line(TO_CHAR(l_count) || ' BIs atualizadas.');

      FOR cApontamento_rec IN cApontamento
      LOOP
          
          UPDATE kcrt_req_header_details rhd 
             SET rhd.visible_parameter3 = replace(rhd.visible_parameter3,l_nome_antigo,l_nome_novo),
                 rhd.last_updated_by = 1,
                 rhd.last_update_date = sysdate            
           WHERE rhd.request_id = cApontamento_rec.request_id;
          
          UPDATE kcrt_requests r
             SET r.last_updated_by = 1,
                 r.last_update_date = sysdate
           WHERE r.request_id = cApontamento_rec.request_id;
           
          UPDATE kcrt_request_details rd
             SET rd.last_updated_by = 1,
                 rd.last_update_date = sysdate
           WHERE rd.request_id = cApontamento_rec.request_id;
          
          l_count := l_count + 1;              
      END LOOP;
      dbms_output.put_line(TO_CHAR(l_count) || ' valorações atualizadas.');
      
  END;
  /